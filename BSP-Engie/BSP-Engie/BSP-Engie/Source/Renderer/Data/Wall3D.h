#pragma once
#include "Mesh.h"
#include "../../Core/Math/Matricies.h"
#include "../Base/Shader.h"

namespace orb
{
	class Wall3D
	{
	private:
		Mesh m_mesh;
	public:
		vec3 position;
		vec3 rotation;
		vec3 scale = vec3(1, 1, 1);
	public:
		Wall3D(vec2 pos1, vec2 pos2, vec2 pos3, vec4 pos4);
		~Wall3D();//de bsp bevat deze walls
	public:
		//void draw(Shader shader, Transform transform, Texture texture);
		void draw(Shader& shader, mat4& projectionMatrix);
	};
}
