#include "Mesh.h"
#include "../Loading/Loader.h"
#include <GL/glew.h>
#include "../../Core/ErrorHandle.h"

orb::Mesh::Mesh()
{
}

orb::Mesh::~Mesh()
{
	//TODO: delete the vao
}

void orb::Mesh::load(std::vector<float> positions, std::vector<float> textureCoords, std::vector<float> normals, std::vector<unsigned int> indicies)
{
	m_vao = generateVAO(positions, textureCoords, normals, indicies);
}

void orb::Mesh::draw()
{
	if (m_vao != NULL)
	{
		GLfnc(glBindVertexArray(m_vao));
		GLfnc(glEnableVertexAttribArray(0));
		GLfnc(glEnableVertexAttribArray(1));
		GLfnc(glEnableVertexAttribArray(2));
		GLfnc(glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, (void*)0));
		GLfnc(glBindVertexArray(0));
	}
}
