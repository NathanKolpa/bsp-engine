#pragma once
#include <vector>


namespace orb
{
	class Mesh
	{
	protected:
		unsigned int m_vao = NULL;
	public:
		Mesh();
		~Mesh();
	public:
		void load(std::vector<float> positions, std::vector<float> textureCoords, std::vector<float> normals, std::vector<unsigned int> indicies);
		void draw();
	};
}