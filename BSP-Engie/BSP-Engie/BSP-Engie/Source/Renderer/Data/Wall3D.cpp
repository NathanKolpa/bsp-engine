#include "Wall3D.h"
#include "../Loading/Loader.h"
#include <GL/glew.h>
#include "../../Core/ErrorHandle.h"



orb::Wall3D::Wall3D(vec2 pos1, vec2 pos2, vec2 pos3, vec4 pos4)
{
	std::vector<float> pos =
	{
		pos1.x, pos1.y, 0.0,
		pos2.x, pos2.y, 0.0,
		pos3.x, pos3.y, 0.0,
		pos4.x, pos4.y, 0.0,
	};
	std::vector<float> tex =
	{
	};

	std::vector<float> norm =
	{
	};

	std::vector<unsigned int> indicies =
	{
		0,1,2,3
	};

	m_mesh.load(pos, tex, norm, indicies);
}


orb::Wall3D::~Wall3D()
{
}

void orb::Wall3D::draw(Shader& shader, mat4& projectionMatrix)
{
	shader.bind();

	mat4 translate;
	mat4 rotationX;
	mat4 rotationY;
	mat4 rotationZ;
	mat4 scaleMat;

	orb::getTransformation(position, rotation, scale, translate, rotationX, rotationY, rotationZ, scaleMat);

	shader.setUniformMat4("uni_translate", translate);
	shader.setUniformMat4("uni_scale", scaleMat);
	shader.setUniformMat4("uni_rotationX", rotationX);
	shader.setUniformMat4("uni_rotationY", rotationY);
	shader.setUniformMat4("uni_rotationZ", rotationZ);

	shader.setUniformMat4("uni_projection", projectionMatrix);

	m_mesh.draw();

	shader.unbind();
}
