#include "Loader.h"
#include "../../Core/ErrorHandle.h"

GLuint storeDataInAttribList(std::vector<float> data, int stride, int vertexCount)//vao has to be bound
{
	//generate VBO
	GLuint vboID;
	GLfnc(glGenBuffers(1, &vboID));

	//bind vbo
	GLfnc(glBindBuffer(GL_ARRAY_BUFFER, vboID));
	GLfnc(glEnableVertexAttribArray(stride));

	//store data
	GLfnc(glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), data.data(), GL_STATIC_DRAW));
	GLfnc(glVertexAttribPointer(stride, vertexCount, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0));

	//undbind vbo
	GLfnc(glBindBuffer(GL_ARRAY_BUFFER, 0));

	return vboID;
}

unsigned int orb::generateVAO(std::vector<float> positions, std::vector<float> textureCoords, std::vector<float> normals, std::vector<unsigned int> indicies)
{
	//generate VAO
	GLuint vaoID;
	GLfnc(glGenVertexArrays(1, &vaoID));
	GLfnc(glBindVertexArray(vaoID));

	//store model data in vao
	storeDataInAttribList(positions, 0, 3);
	storeDataInAttribList(textureCoords, 1, 2);
	storeDataInAttribList(normals, 2, 3);

	//create a index buffer
	GLuint eboID;
	GLfnc(glGenBuffers(1, &eboID));
	GLfnc(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboID));
	GLfnc(glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicies.size() * sizeof(float), indicies.data(), GL_STATIC_DRAW));


	//unbind and return
	GLfnc(glBindVertexArray(0));
	return vaoID;
}