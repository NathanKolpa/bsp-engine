#pragma once
#include <vector>
namespace orb
{
	unsigned int generateVAO(std::vector<float> positions, std::vector<float> textureCoords, std::vector<float> normals, std::vector<unsigned int> indicies);
}