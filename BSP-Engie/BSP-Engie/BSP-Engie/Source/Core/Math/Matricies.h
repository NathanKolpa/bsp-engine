#pragma once
#include "Vectors.h"
#include <cmath>

struct mat2
{

};

struct mat3
{

};

struct mat4
{

	mat4(float fill = 0)
	{
		if (fill != 0)
			for (int y = 0; y < 4; y++)
				for (int x = 0; x < 4; x++)
				{
					matrix[x][y] = fill;
				}
	}
	~mat4()
	{
	}
	float matrix[4][4] = { 0 };

	mat4 operator*(float b)
	{
		mat4 ans;
		for (int y = 0; y < 4; y++)
			for (int x = 0; x < 4; x++)
			{
				ans.matrix[x][y] *= b;
			}
		return ans;
	}

	mat4& operator*=(float b)
	{
		for (int y = 0; y < 4; y++)
			for (int x = 0; x < 4; x++)
			{
				this->matrix[x][y] *= b;
			}
		return *this;
	}

	vec4 operator*(const vec4& b)
	{
		vec4 ans;
		ans.x = b.x * this->matrix[0][0] + b.y * this->matrix[1][0] + b.z * this->matrix[2][0] + b.w * this->matrix[3][0];
		ans.y = b.x * this->matrix[0][1] + b.y * this->matrix[1][1] + b.z * this->matrix[2][1] + b.w * this->matrix[3][1];
		ans.z = b.x * this->matrix[0][2] + b.y * this->matrix[1][2] + b.z * this->matrix[2][2] + b.w * this->matrix[3][2];
		ans.w = b.x * this->matrix[0][3] + b.y * this->matrix[1][3] + b.z * this->matrix[2][3] + b.w * this->matrix[3][3];
		return ans;
	}

	vec3 operator*(const vec3& b)
	{
		vec3 ans;
		ans.x = b.x * this->matrix[0][0] + b.y * this->matrix[1][0] + b.z * this->matrix[2][0];
		ans.y = b.x * this->matrix[0][1] + b.y * this->matrix[1][1] + b.z * this->matrix[2][1];
		ans.z = b.x * this->matrix[0][2] + b.y * this->matrix[1][2] + b.z * this->matrix[2][2];
		float w = b.x * this->matrix[0][3] + b.y * this->matrix[1][3] + b.z * this->matrix[3][3];
		if (w != 0.0f)
		{
			ans.x /= w;
			ans.y /= w;
			ans.z /= w;
		}
		return ans;
	}
};

namespace orb
{
	static void getTransformation(vec3& position, vec3& rotation, vec3& scalar, mat4& translate, mat4& rotationX, mat4& rotationY, mat4& rotationZ, mat4& scale)
	{
		//translate
		translate.matrix[0][0] = 1;                     //[   1,   0,   0,   0]
		translate.matrix[1][1] = 1;                     //[   0,   1,   0,   0]
		translate.matrix[2][2] = 1;                     //[   0,   0,   1,   0]
		translate.matrix[3][3] = 1;                     //[   x,   y,   z,   1]
		translate.matrix[3][0] = position.x;
		translate.matrix[3][1] = position.y;
		translate.matrix[3][2] = position.z;

		//scaling
		scale.matrix[0][0] = scalar.x;                  //[   x,   0,   0,   0]
		scale.matrix[1][1] = scalar.y;                  //[   0,   y,   0,   0]
		scale.matrix[2][2] = scalar.z;                  //[   0,   0,   z,   0]
		scale.matrix[3][3] = 1;                         //[   0,   0,   0,   1]

		//rotation
		float theada = 3.14159f / 180.0f;
		vec3 radRotation = rotation;
		radRotation.x *= theada;
		radRotation.y *= theada;
		radRotation.z *= theada;

		rotationX.matrix[0][0] = 1;
		rotationX.matrix[1][1] = cosf(radRotation.x);   //[  1,    0,   0,   0]
		rotationX.matrix[1][2] = -sinf(radRotation.x);  //[  0,  cos,-sin,   0]
		rotationX.matrix[2][1] = sinf(radRotation.x);   //[  0,  sin, cos,   0]
		rotationX.matrix[2][2] = cosf(radRotation.x);   //[  0,    0,   0,   1]
		rotationX.matrix[3][3] = 1;

		rotationY.matrix[0][0] = cosf(radRotation.y);   //[ cos,   0, sin,   0]
		rotationY.matrix[0][2] = sinf(radRotation.y);   //[   0,   0,   0,   0]
		rotationY.matrix[1][1] = 1;                     //[-sin,   0, cos,   0]
		rotationY.matrix[2][0] = -sinf(radRotation.y);  //[   0,   0,   0,   0]
		rotationY.matrix[2][2] = cosf(radRotation.y);
		rotationY.matrix[3][3] = 1;

		rotationZ.matrix[0][0] = cosf(radRotation.z);   //[ cos,-sin,   0,   0]
		rotationZ.matrix[0][1] = -sinf(radRotation.z);  //[ sin, cos,   0,   0]
		rotationZ.matrix[1][0] = sinf(radRotation.z);   //[   0,   0,   1,   0]
		rotationZ.matrix[1][1] = cosf(radRotation.z);   //[   0,   0,   0,   1]
		rotationZ.matrix[2][2] = 1;
		rotationZ.matrix[3][3] = 1;



	}
	static mat4 ProjectionMatrix(int WindowWidth, int WindowHeight, float FOV, float Far, float Near)
	{

		mat4 ans;
		float AspectRatio = (float)WindowHeight / (float)WindowWidth;
		float FovRadiants = 1.0f / tanf(FOV * 0.5f / 180.0f * 3.14159f);

		ans.matrix[0][0] = AspectRatio * FovRadiants;           //[            a * fov,                  0,                  0,                  0]
		ans.matrix[1][1] = FovRadiants;                         //[                  0,                fov,                  0,                  0]
		ans.matrix[2][2] = Far / (Far - Near);                  //[                  0,                  0,          far / dis,                  1]
		ans.matrix[2][3] = 1.0f;                                //[                  0,                  0,(-far * near) / dis,                  0]
		ans.matrix[3][2] = (-Far * Near) / (Far - Near);		
		return ans;
	}
}