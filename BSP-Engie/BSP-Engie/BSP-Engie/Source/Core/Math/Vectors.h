#pragma once

//struct moet typedef zijn

struct vec3;
struct vec4;

struct vec2
{
	vec2(float X = 0, float Y = 0)
		:x(X), y(Y){}

	float x, y;

	//swizzeling
	inline vec2& operator=(const float b) { this->x = b; this->y = b; return *this; }
	inline vec2& operator=(const vec3& b);
	inline vec2& operator=(const vec4& b);

	//vec2 + vec2
	inline vec2 operator+(const vec2& b) { vec2 ans(this->x + b.x, this->y + b.y); return ans; }
	inline vec2 operator-(const vec2& b) { vec2 ans(this->x - b.x, this->y - b.y); return ans; }
	inline vec2 operator*(const vec2& b) { vec2 ans(this->x * b.x, this->y * b.y); return ans; }
	inline vec2 operator/(const vec2& b) { vec2 ans(this->x / b.x, this->y / b.y); return ans; }

	inline vec2& operator+=(const vec2& b) { this->x += b.x; this->y += b.y; return *this; }
	inline vec2& operator-=(const vec2& b) { this->x -= b.x; this->y -= b.y; return *this; }
	inline vec2& operator*=(const vec2& b) { this->x *= b.x; this->y *= b.y; return *this; }
	inline vec2& operator/=(const vec2& b) { this->x /= b.x; this->y /= b.y; return *this; }

	//vec2 + float
	inline vec2 operator+(const float b) { vec2 ans(this->x + b, this->y + b); return ans; }
	inline vec2 operator-(const float b) { vec2 ans(this->x - b, this->y - b); return ans; }
	inline vec2 operator*(const float b) { vec2 ans(this->x * b, this->y * b); return ans; }
	inline vec2 operator/(const float b) { vec2 ans(this->x / b, this->y / b); return ans; }

	inline vec2& operator+=(const float b) { this->x += b; this->y += b; return *this; }
	inline vec2& operator-=(const float b) { this->x -= b; this->y -= b; return *this; }
	inline vec2& operator*=(const float b) { this->x *= b; this->y *= b; return *this; }
	inline vec2& operator/=(const float b) { this->x /= b; this->y /= b; return *this; }

	//vec2 + vec3
	inline vec2 operator+(const vec3& b);
	inline vec2 operator-(const vec3& b);
	inline vec2 operator*(const vec3& b);
	inline vec2 operator/(const vec3& b);

	inline vec2& operator+=(const vec3& b);
	inline vec2& operator-=(const vec3& b);
	inline vec2& operator*=(const vec3& b);
	inline vec2& operator/=(const vec3& b);

	//vec2 + vec4
	inline vec2 operator+(const vec4& b);
	inline vec2 operator-(const vec4& b);
	inline vec2 operator*(const vec4& b);
	inline vec2 operator/(const vec4& b);

	inline vec2& operator+=(const vec4& b);
	inline vec2& operator-=(const vec4& b);
	inline vec2& operator*=(const vec4& b);
	inline vec2& operator/=(const vec4& b);

};

struct vec3
{
	vec3(float X = 0, float Y = 0, float Z = 0)
		:x(X), y(Y), z(Z){}

	float x, y, z;

	//swizzeleing
	inline vec3& operator=(const float b) { this->x = b; this->y = b; this->z = b; return *this; }
	inline vec3& operator=(const vec2& b) { this->x = b.x; this->y = b.y; this->z = 0; return *this; }
	inline vec3& operator=(const vec4& b);
	//vec3 + vec3
	inline vec3 operator+(const vec3& b) { vec3 ans(this->x + b.x, this->y + b.y, this->z + b.z); return ans; }
	inline vec3 operator-(const vec3& b) { vec3 ans(this->x - b.x, this->y - b.y, this->z - b.z); return ans; }
	inline vec3 operator*(const vec3& b) { vec3 ans(this->x * b.x, this->y * b.y, this->z * b.z); return ans; }
	inline vec3 operator/(const vec3& b) { vec3 ans(this->x / b.x, this->y / b.y, this->z / b.z); return ans; }
	   
	inline vec3& operator+=(const vec3& b) { this->x += b.x; this->y += b.y; this->z += b.z; return *this; }
	inline vec3& operator-=(const vec3& b) { this->x -= b.x; this->y -= b.y; this->z -= b.z; return *this; }
	inline vec3& operator*=(const vec3& b) { this->x *= b.x; this->y *= b.y; this->z *= b.z; return *this; }
	inline vec3& operator/=(const vec3& b) { this->x /= b.x; this->y /= b.y; this->z /= b.z; return *this; }
	   
	//vec3 + float
	inline vec3 operator+(const float b) { vec3 ans(this->x + b, this->y + b, this->z + b); return ans; }
	inline vec3 operator-(const float b) { vec3 ans(this->x - b, this->y - b, this->z - b); return ans; }
	inline vec3 operator*(const float b) { vec3 ans(this->x * b, this->y * b, this->z * b); return ans; }
	inline vec3 operator/(const float b) { vec3 ans(this->x / b, this->y / b, this->z / b); return ans; }
	   
	inline vec3& operator+=(const float b) { this->x += b; this->y += b; this->z += b; return *this; }
	inline vec3& operator-=(const float b) { this->x -= b; this->y -= b; this->z -= b; return *this; }
	inline vec3& operator*=(const float b) { this->x *= b; this->y *= b; this->z *= b; return *this; }
	inline vec3& operator/=(const float b) { this->x /= b; this->y /= b; this->z /= b; return *this; }

	//vec3 + vec2
	inline vec3 operator+(const vec2& b) { vec3 ans(this->x + b.x, this->y + b.y, this->z); return ans; }
	inline vec3 operator-(const vec2& b) { vec3 ans(this->x - b.x, this->y - b.y, this->z); return ans; }
	inline vec3 operator*(const vec2& b) { vec3 ans(this->x * b.x, this->y * b.y, this->z); return ans; }
	inline vec3 operator/(const vec2& b) { vec3 ans(this->x / b.x, this->y / b.y, this->z); return ans; }

	inline vec3& operator+=(const vec2& b) { this->x += b.x; this->y += b.y; return *this; }
	inline vec3& operator-=(const vec2& b) { this->x -= b.x; this->y -= b.y; return *this; }
	inline vec3& operator*=(const vec2& b) { this->x *= b.x; this->y *= b.y; return *this; }
	inline vec3& operator/=(const vec2& b) { this->x /= b.x; this->y /= b.y; return *this; }

	//vec3 + vec4
	inline vec3 operator+(const vec4& b);
	inline vec3 operator-(const vec4& b);
	inline vec3 operator*(const vec4& b);
	inline vec3 operator/(const vec4& b);

	inline vec3& operator+=(const vec4& b);
	inline vec3& operator-=(const vec4& b);
	inline vec3& operator*=(const vec4& b);
	inline vec3& operator/=(const vec4& b);

};


struct vec4
{
	vec4(float X = 0, float Y = 0, float Z = 0, float W = 0)
		:x(X), y(Y), z(Z), w(W)
	{}


	float x, y, z, w;

	//swizzeling
	inline vec4& operator=(const float b) { this->x = b; this->y = b; this->z = b; this->w = b; return *this; }
	inline vec4& operator=(const vec2& b) { this->x = b.x; this->y = b.y; this->z = 0; return *this; }
	inline vec4& operator=(const vec3& b) { this->x = b.x; this->y = b.y; this->z = b.z; this->w = 0; return *this; }
	//vec4 + vec4
	inline vec4 operator+(const vec4& b) { vec4 ans(this->x + b.x, this->y + b.y, this->z + b.z, this->w + b.w); return ans; }
	inline vec4 operator-(const vec4& b) { vec4 ans(this->x - b.x, this->y - b.y, this->z - b.z, this->w - b.w); return ans; }
	inline vec4 operator*(const vec4& b) { vec4 ans(this->x * b.x, this->y * b.y, this->z * b.z, this->w * b.w); return ans; }
	inline vec4 operator/(const vec4& b) { vec4 ans(this->x / b.x, this->y / b.y, this->z / b.z, this->w / b.w); return ans; }
	
	inline vec4& operator+=(const vec4& b) { this->x += b.x; this->y += b.y; this->z += b.z; this->w += b.w; return *this; }
	inline vec4& operator-=(const vec4& b) { this->x -= b.x; this->y -= b.y; this->z -= b.z; this->w -= b.w; return *this; }
	inline vec4& operator*=(const vec4& b) { this->x *= b.x; this->y *= b.y; this->z *= b.z; this->w *= b.w; return *this; }
	inline vec4& operator/=(const vec4& b) { this->x /= b.x; this->y /= b.y; this->z /= b.z; this->w /= b.w; return *this; }
	
	//vec4 + float
	inline vec4 operator+(const float b) { vec4 ans(this->x + b, this->y + b, this->z + b, this->w + b); return ans; }
	inline vec4 operator-(const float b) { vec4 ans(this->x - b, this->y - b, this->z - b, this->w - b); return ans; }
	inline vec4 operator*(const float b) { vec4 ans(this->x * b, this->y * b, this->z * b, this->w * b); return ans; }
	inline vec4 operator/(const float b) { vec4 ans(this->x / b, this->y / b, this->z / b, this->w / b); return ans; }
	
	inline vec4& operator+=(const float b) { this->x += b; this->y += b; this->z += b; this->w += b; return *this; }
	inline vec4& operator-=(const float b) { this->x -= b; this->y -= b; this->z -= b; this->w -= b; return *this; }
	inline vec4& operator*=(const float b) { this->x *= b; this->y *= b; this->z *= b; this->w *= b; return *this; }
	inline vec4& operator/=(const float b) { this->x /= b; this->y /= b; this->z /= b; this->w /= b; return *this; }

	//vec4 + vec2
	inline vec4 operator+(const vec2& b) { vec4 ans(this->x + b.x, this->y + b.y, this->z, this->w); return ans; }
	inline vec4 operator-(const vec2& b) { vec4 ans(this->x - b.x, this->y - b.y, this->z, this->w); return ans; }
	inline vec4 operator*(const vec2& b) { vec4 ans(this->x * b.x, this->y * b.y, this->z, this->w); return ans; }
	inline vec4 operator/(const vec2& b) { vec4 ans(this->x / b.x, this->y / b.y, this->z, this->w); return ans; }

	inline vec4& operator+=(const vec2& b) { this->x += b.x; this->y += b.y; return *this; }
	inline vec4& operator-=(const vec2& b) { this->x -= b.x; this->y -= b.y; return *this; }
	inline vec4& operator*=(const vec2& b) { this->x *= b.x; this->y *= b.y; return *this; }
	inline vec4& operator/=(const vec2& b) { this->x /= b.x; this->y /= b.y; return *this; }

	//vec4 + vec3
	inline vec4 operator+(const vec3& b) { vec4 ans(this->x + b.x, this->y + b.y, this->z + b.z, this->w); return ans; }
	inline vec4 operator-(const vec3& b) { vec4 ans(this->x - b.x, this->y - b.y, this->z - b.z, this->w); return ans; }
	inline vec4 operator*(const vec3& b) { vec4 ans(this->x * b.x, this->y * b.y, this->z * b.z, this->w); return ans; }
	inline vec4 operator/(const vec3& b) { vec4 ans(this->x / b.x, this->y / b.y, this->z / b.z, this->w); return ans; }

	inline vec4& operator+=(const vec3& b) { this->x += b.x; this->y += b.y; this->z += b.z; return *this; }
	inline vec4& operator-=(const vec3& b) { this->x -= b.x; this->y -= b.y; this->z -= b.z; return *this; }
	inline vec4& operator*=(const vec3& b) { this->x *= b.x; this->y *= b.y; this->z *= b.z; return *this; }
	inline vec4& operator/=(const vec3& b) { this->x /= b.x; this->y /= b.y; this->z /= b.z; return *this; }

};


//vec2 stuff
vec2& vec2::operator=(const vec3& b) { this->x = b.x; this->y = b.y; return *this; }
vec2& vec2::operator=(const vec4& b) { this->x = b.x; this->y = b.y; return *this; }

//vec2 + vec3
vec2 vec2::operator+(const vec3& b) { vec2 ans(this->x + b.x, this->y + b.y); return ans; }
vec2 vec2::operator-(const vec3& b) { vec2 ans(this->x - b.x, this->y - b.y); return ans; }
vec2 vec2::operator*(const vec3& b) { vec2 ans(this->x * b.x, this->y * b.y); return ans; }
vec2 vec2::operator/(const vec3& b) { vec2 ans(this->x / b.x, this->y / b.y); return ans; }

vec2& vec2::operator+=(const vec3& b) { this->x += b.x; this->y += b.y; return *this; }
vec2& vec2::operator-=(const vec3& b) { this->x -= b.x; this->y -= b.y; return *this; }
vec2& vec2::operator*=(const vec3& b) { this->x *= b.x; this->y *= b.y; return *this; }
vec2& vec2::operator/=(const vec3& b) { this->x /= b.x; this->y /= b.y; return *this; }

//vec2 + vec4
vec2 vec2::operator+(const vec4& b) { vec2 ans(this->x + b.x, this->y + b.y); return ans; }
vec2 vec2::operator-(const vec4& b) { vec2 ans(this->x - b.x, this->y - b.y); return ans; }
vec2 vec2::operator*(const vec4& b) { vec2 ans(this->x * b.x, this->y * b.y); return ans; }
vec2 vec2::operator/(const vec4& b) { vec2 ans(this->x / b.x, this->y / b.y); return ans; }

vec2& vec2::operator+=(const vec4& b) { this->x += b.x; this->y += b.y; return *this; }
vec2& vec2::operator-=(const vec4& b) { this->x -= b.x; this->y -= b.y; return *this; }
vec2& vec2::operator*=(const vec4& b) { this->x *= b.x; this->y *= b.y; return *this; }
vec2& vec2::operator/=(const vec4& b) { this->x /= b.x; this->y /= b.y; return *this; }

//vec3 stuff
vec3& vec3::operator=(const vec4& b) { this->x = b.x; this->y = b.y; this->z = b.z; return *this; }

//vec3 + vec4
vec3 vec3::operator+(const vec4& b) { vec3 ans(this->x + b.x, this->y + b.y, this->z + b.z); return ans; }
vec3 vec3::operator-(const vec4& b) { vec3 ans(this->x - b.x, this->y - b.y, this->z - b.z); return ans; }
vec3 vec3::operator*(const vec4& b) { vec3 ans(this->x * b.x, this->y * b.y, this->z * b.z); return ans; }
vec3 vec3::operator/(const vec4& b) { vec3 ans(this->x / b.x, this->y / b.y, this->z / b.z); return ans; }

vec3& vec3::operator+=(const vec4& b) { this->x += b.x; this->y += b.y; this->z += b.z; return *this; }
vec3& vec3::operator-=(const vec4& b) { this->x -= b.x; this->y -= b.y; this->z -= b.z; return *this; }
vec3& vec3::operator*=(const vec4& b) { this->x *= b.x; this->y *= b.y; this->z *= b.z; return *this; }
vec3& vec3::operator/=(const vec4& b) { this->x /= b.x; this->y /= b.y; this->z /= b.z; return *this; }