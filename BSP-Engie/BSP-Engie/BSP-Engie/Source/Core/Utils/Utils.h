#pragma once
#include "../Math/Vectors.h"
#include <cmath>

namespace orb
{
	static inline double Det(double a, double b, double c, double d)
	{
		return a * d - b * c;
	}

	///Calculate intersection of two lines.
	///\return true if found, false if not found or error
	static vec2 LineLineIntersect(vec2 A1, vec2 A2, vec2 B1, vec2 B2)
	{
		vec2 output;
		double	 x1 = A1.x, y1 = A1.y, //Line 1 start
			x2 = A2.x, y2 = A2.y, //Line 1 end
			x3 = B1.x, y3 = B1.y, //Line 2 start
			x4 = B2.x, y4 = B2.y;


		double detL1 = Det(x1, y1, x2, y2);
		double detL2 = Det(x3, y3, x4, y4);
		double x1mx2 = x1 - x2;
		double x3mx4 = x3 - x4;
		double y1my2 = y1 - y2;
		double y3my4 = y3 - y4;

		double xnom = Det(detL1, x1mx2, detL2, x3mx4);
		double ynom = Det(detL1, y1my2, detL2, y3my4);
		double denom = Det(x1mx2, y1my2, x3mx4, y3my4);
		if (denom == 0.0)//Lines don't seem to cross
		{
			output.x = NAN;
			output.x = NAN;
			return false;
		}

		output.x = xnom / denom;
		output.y = ynom / denom;
		if (!isfinite(output.x) || !isfinite(output.y)) //Probably a numerical issue
			return false;

		return output; //All OK
	}

	static int getSideOfPoint(vec2 lineA, vec2 lineB, vec2 point)
	{
		float outcome = ((lineB.x - lineA.x) * (point.y - lineA.y) - (lineB.y - lineA.y) * (point.x - lineA.x));

		if (outcome == 0)
			return ORB_MIDDLE;
		else if (outcome > 0)
			return ORB_LEFT;
		else if (outcome < 0)
			return ORB_RIGHT;
	}
}