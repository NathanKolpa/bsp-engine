#pragma once
#include <string>

const std::string indent = "	";


const std::string textureData = "[index, filename]";

const std::string fileStructure =
"<BSPmap>"
"	<meta>"
"		<texturedata>"
"		</texturedata>"
"	</meta>"
"	<shaders>"
"		<vertexshader>"
"		</vertexshader>"
"		<fragmentshader>"
"		</fragmentshader>"
"	</shaders>"
"	<texture>"
"	</texture>"
"	<bsp>"
"	</bsp>"
"</BSPmap>";