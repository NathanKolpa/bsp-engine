#include "FileParser.h"
#include "../../Core/ErrorHandle.h"
#include <fstream>
#include <sstream>

//the index must equal a '<'

orb::FileParser::FileParser()
{
	parse(fileStructure);
}

orb::FileParser::FileParser(std::string fileName)
{
	read(fileName);
}


orb::FileParser::~FileParser()
{
}

std::string getTagName(const std::string& data, int index)
{
	std::string ans;
	for (int i = index + 1; i < data.length(); i++)
	{
		if (data[i] == '>')
			break;

		if(data[i] != '/')
			ans += data[i];

	}
	return ans;
}

void orb::FileParser::parseTag(const std::string& data, int index, int parent)
{
	if (data[index] != '<')
	{
		logWarning("invalid index");
		return;
	}
	
	int dataIndex = m_currentData.size();
	m_currentData.push_back(orb::tag());

	orb::tag currentTag;
	currentTag.openChar = index;
	currentTag.tagName = getTagName(data, index);
	currentTag.parentIndex = parent;

	//goto the end of the tag
	for (int i = index + 1 + currentTag.tagName.length() + 1; i < data.length(); i++)
	{
		if (data[i] == '<')//check for a tag
		{
			if (data[i + 1] == '/')//check if the tag is closing
			{
				if (getTagName(data, i) == currentTag.tagName)
				{
					m_currentStack--;
					currentTag.closeChar = i;
					goto BREAK;
				}
			}
			else//if not than another tag is found
			{
				//if another tag is found then parse that tag
				m_currentStack++;
				currentTag.children.push_back(m_currentData.size());
				parseTag(data, i, dataIndex);
				i = m_currentData.back().closeChar + 3 + m_currentData.back().tagName.length();
			}
		}

		else//if no tag is found then add the text to the contents
		{
			if(data[i] != '\t' && data[i] != '\n' && data[i] != indent[0])
				currentTag.contents += data[i];
		}
	}

BREAK:

	if (currentTag.closeChar == -1)
	{
		logError("missing close tag for: <" << currentTag.tagName << ">");
	}
	else
	{
		m_currentData[dataIndex] = currentTag;
	}

}

void orb::FileParser::parse(const std::string& xmlData)
{
	//seach for a '<'
	for (int i = 0; i < xmlData.length(); i++)
	{
		if (xmlData[i] == '<')
		{
			m_currentStack = 0;
			m_currentData.clear();
			parseTag(xmlData, 0, 0);
			if (m_currentStack != 0)
			{
				logWarning("the xml loading may have caused errors");
			}
			m_currentData[0].contents = "";//content for the main tag is invalid
			break;
		}
	}

}

void orb::FileParser::getTagString(std::string& out, int index)
{

	out += "<";
	out += m_currentData[index].tagName;
	out += ">\n";

	out += indent;
	out += m_currentData[index].contents;
	out += "\n";

	for (int i = 0; i < m_currentData[index].children.size(); i++)
	{
		m_currentStack++;
		getTagString(out, m_currentData[index].children[i]);
		m_currentStack--;
	}

	out += "</";
	out += m_currentData[index].tagName;
	out += ">\n";
}

std::string orb::FileParser::toString()
{
	std::string ans;

	m_currentStack = 0;
	getTagString(ans, 0);

	return ans;
}

void orb::FileParser::read(std::string fileName)
{
	std::ifstream file(fileName);
	if (!file.good())
	{
		logError("could not open file " << fileName);
	}
	else
	{
		logMessage("opened file: " << fileName);
	}

	std::stringstream buffer;
	buffer << file.rdbuf();

	parse(buffer.str());
}

void orb::FileParser::write(std::string fileName)
{
	std::string output = toString();
	std::ofstream file(fileName + ".bsp");
	file << output;
	file.close();
	logMessage("created/overwritten \"" << fileName << ".bsp\"");
}
