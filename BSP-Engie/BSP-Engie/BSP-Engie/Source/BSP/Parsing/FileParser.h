#pragma once
#include "FileStructure.h"
#include <vector>

//TODO: delete more then one more space
//TODO: better parsing
namespace orb
{
	struct tag
	{
		std::string tagName;
		std::string contents;
		int openChar = 0;
		int closeChar = 0;

		int parentIndex = -1;
		std::vector<int> children = {};
	};


	class FileParser
	{
	private:
		std::vector<tag> m_currentData;
		int m_currentStack = 0;
	public:
		FileParser();
		FileParser(std::string fileName);
		~FileParser();
	private:
		void parseTag(const std::string& data, int index, int parent);
		void getTagString(std::string& out, int index);
	public:
		void read(std::string fileName);
		void write(std::string fileName);
		void parse(const std::string& xmlData);
		std::string toString();
	};
}

