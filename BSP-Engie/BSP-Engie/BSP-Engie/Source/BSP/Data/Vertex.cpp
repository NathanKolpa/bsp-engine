#include "Vertex.h"

orb::Vertex::Vertex()
{
}

orb::Vertex::Vertex(vec2 pos)
{
	m_position = pos;
}

vec2 orb::Vertex::getPosition()
{
	return m_position;
}

void orb::Vertex::setPosition(vec2 pos)
{
	m_position = pos;
}

bool orb::Vertex::mouseIsIntersecting(GLFWwindow* window, int width, vec2 offset, float zoom)
{
	float halfWidth = (float)width / 2;

	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);
	vec2 transformedPos(((xpos + offset.x) / zoom), ((ypos + offset.y) / zoom));

	if (transformedPos.x > m_position.x - halfWidth && transformedPos.x < m_position.x + halfWidth
		&& transformedPos.y > m_position.y - halfWidth && transformedPos.y < m_position.y + halfWidth)
	{
		return true;
	}

	return false;
}
