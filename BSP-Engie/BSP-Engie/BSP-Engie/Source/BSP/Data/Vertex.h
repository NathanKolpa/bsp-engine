#pragma once
#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "../../Core/Math/Vectors.h"
namespace orb
{

	class Vertex
	{
	private:
		vec2 m_position;
	public:
		Vertex();
		Vertex(vec2 pos);
	public:
		vec2 getPosition();
		void setPosition(vec2 pos);
		bool mouseIsIntersecting(GLFWwindow* window, int width, vec2 offset, float zoom);
	};
}