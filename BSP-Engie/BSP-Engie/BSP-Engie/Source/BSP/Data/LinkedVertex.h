#pragma once
#include "Vertex.h"

namespace orb
{
	class LinkedVertex
	{
	private:
		Vertex* m_vertex_p = nullptr;
		Vertex m_vertex;
	public:
		void setVertex(Vertex* vertexP)
		{
			m_vertex_p = vertexP;
		}

		void setVertex(Vertex vertex)
		{
			m_vertex = vertex;
		}

		orb::Vertex getVertex()
		{
			if (m_vertex_p != nullptr)
				return *m_vertex_p;
			else
				return m_vertex;
		}

		orb::Vertex* getVertexPointer()
		{
			if (m_vertex_p != nullptr)
				return m_vertex_p;
			else
				return &m_vertex;
		}
	};
}