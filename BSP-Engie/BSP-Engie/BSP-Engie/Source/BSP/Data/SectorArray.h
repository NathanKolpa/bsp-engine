#pragma once
#include <list>
#include "Sector.h"

namespace orb
{
	class SectorArray
	{
	private:
		std::list<Sector> m_sectors;
	public:
		Sector& operator[] (const int index);
		int size();
		void push_back(Sector sector);
	};
}