#include "SectorArray.h"

orb::Sector& orb::SectorArray::operator[](const int index)
{
	{
		if (m_sectors.size() > index)
		{
			std::list<Sector>::iterator it = m_sectors.begin();
			std::advance(it, index);
			return (*it);
		}
	}
}

int orb::SectorArray::size()
{
	return m_sectors.size();
}

void orb::SectorArray::push_back(Sector sector)
{
	m_sectors.push_back(sector);
}
