#include "Sector.h"
#include <GL/glew.h>
#include "../../Core/ErrorHandle.h"

orb::Sector::Sector(std::vector<LinkedVertex> verticies)
{
	m_verticies = verticies;
}

orb::Sector::~Sector()
{
}

std::vector<orb::LinkedVertex> orb::Sector::getVerticies()
{
	return m_verticies;
}

orb::LinkedVertex* orb::Sector::getInterSectingVertex(GLFWwindow * window, int width, vec2 offset, float zoom)
{
	for (int i = 0; i < m_verticies.size(); i++)
	{
		if (m_verticies[i].getVertex().mouseIsIntersecting(window, width, offset, zoom))
		{
			return &m_verticies[i];
		}
	}

	return nullptr;
}

void orb::Sector::draw(int windowWidth, int windowHeight, vec2 offset, float zoom, int hitboxWidth)
{
	if (m_verticies.size() > 0)
	{
		GLfnc(glLineWidth(3));
		GLfnc(glViewport(0, 0, windowWidth, windowHeight));
		GLfnc(glLoadIdentity());
		GLfnc(glOrtho(0, windowWidth, windowHeight, 0, 0, 1));
		GLfnc(glMatrixMode(GL_MODELVIEW));
		
		glColor4f(1, 1, 1, 1);
		glBegin(GL_LINE_LOOP);

		for (int i = 0; i < m_verticies.size(); i++)
		{
			glVertex3f((m_verticies[i].getVertex().getPosition().x) * zoom - offset.x, (m_verticies[i].getVertex().getPosition().y) * zoom - offset.y, 0);
		}

		glEnd();

		glColor4f(1, 1, 1, 1);
		glBegin(GL_QUADS);
		for (int i = 0; i < m_verticies.size(); i++)
		{
			float halfWidth = hitboxWidth / 2;

			glVertex3f((m_verticies[i].getVertex().getPosition().x - halfWidth) * zoom - offset.x, (m_verticies[i].getVertex().getPosition().y - halfWidth) * zoom - offset.y, 0);
			glVertex3f((m_verticies[i].getVertex().getPosition().x - halfWidth) * zoom - offset.x, (m_verticies[i].getVertex().getPosition().y + halfWidth) * zoom - offset.y, 0);
			glVertex3f((m_verticies[i].getVertex().getPosition().x + halfWidth) * zoom - offset.x, (m_verticies[i].getVertex().getPosition().y + halfWidth) * zoom - offset.y, 0);
			glVertex3f((m_verticies[i].getVertex().getPosition().x + halfWidth) * zoom - offset.x, (m_verticies[i].getVertex().getPosition().y - halfWidth) * zoom - offset.y, 0);

		}
		glEnd();

		GLfnc(glFlush());
	}
}