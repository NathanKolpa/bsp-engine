#pragma once
#include "LinkedVertex.h"

namespace orb
{
	class Sector
	{
	private:
		std::vector<LinkedVertex> m_verticies;
	public:
		Sector(std::vector<LinkedVertex> verticies);
		~Sector();
	public:
		void setVertex(int index, LinkedVertex value);
		LinkedVertex getVertex(int index);
		std::vector<LinkedVertex> getVerticies();
		
		LinkedVertex* getInterSectingVertex(GLFWwindow* window, int width, vec2 offset, float zoom);

		void draw(int windowWidth, int windowHeight, vec2 offset, float zoom, int hitboxWidth);
	};
}