#include "../Editor.h"
#include "../../../Core/ErrorHandle.h"
#include "../../../Core/Utils/Time.h"
#include <algorithm>

void orb::Editor::keyboardInput()
{

	if (glfwGetKey(m_window->getWindowPointer(), GLFW_KEY_W) && offset.y > 0)
	{
		offset.y -= 500 * (float)Time::deltaTime();
	}
	if (glfwGetKey(m_window->getWindowPointer(), GLFW_KEY_S))
	{
		offset.y += 500 * (float)Time::deltaTime();
	}
	if (glfwGetKey(m_window->getWindowPointer(), GLFW_KEY_D))
	{
		offset.x += 500 * (float)Time::deltaTime();
	}
	if (glfwGetKey(m_window->getWindowPointer(), GLFW_KEY_A) && offset.x > 0)
	{
		offset.x -= 500 * (float)Time::deltaTime();
	}

	if (glfwGetKey(m_window->getWindowPointer(), GLFW_KEY_E))
	{
		m_zoom += 0.2 * (float)Time::deltaTime();
	}
	if (glfwGetKey(m_window->getWindowPointer(), GLFW_KEY_Q) && m_zoom > 0.1)
	{
		m_zoom -= 0.2 * (float)Time::deltaTime();
	}
}



float roundToHundredths(float x)
{
	x /= 100;
	return floor(x + 0.5) * 100;
}

void orb::Editor::_actionPlaceWall()
{
	if (m_mode == ORB_EDITM_LINE_EDIT)
	{
		//check if the the first one is clicked
		if (m_temporaryVertices.size() > 0)
		{
			if (m_temporaryVertices[0].getVertex().mouseIsIntersecting(m_window->getWindowPointer(), m_cellSize, offset, m_zoom))
			{
				_actionAddSector();
				_actionToggleLineEditing();
				return;
			}
		}

		double xpos, ypos;
		glfwGetCursorPos(m_window->getWindowPointer(), &xpos, &ypos);
		vec2 transformedPos(roundToHundredths((xpos + offset.x) / m_zoom), roundToHundredths((ypos + offset.y) / m_zoom));

		LinkedVertex Lvertex;

		//check if there is a vertex that intersects and if so then use that vertex instead
		Vertex vertex(vec2(transformedPos.x, transformedPos.y));
		Lvertex.setVertex(vertex);

		for (int i = 0; i < m_sectors.size(); i++)
		{
			LinkedVertex* current = m_sectors[i].getInterSectingVertex(m_window->getWindowPointer(), m_cellSize, offset, m_zoom);
			if (current != nullptr)
			{
				Lvertex.setVertex(current->getVertexPointer());
				break;
			}
		}


		m_temporaryVertices.push_back(Lvertex);
		logMessage("added vertex x" << Lvertex.getVertex().getPosition().x << " y" << Lvertex.getVertex().getPosition().y);
	}
}

void orb::Editor::_actionToggleLineEditing()
{
	if (m_mode == ORB_EDITM_NORMAL)
	{
		m_mode = ORB_EDITM_LINE_EDIT;
		logMessage("now entering sector edit mode");
	}
	else
	{
		m_mode = ORB_EDITM_NORMAL;
		m_temporaryVertices.clear();
		logMessage("now exiting sector edit mode");
	}
}

void orb::Editor::_actionAddSector()
{

	m_sectors.push_back(Sector(m_temporaryVertices));
	logMessage("added sector with " << m_temporaryVertices.size() << " verticies");
	m_temporaryVertices.clear();
}

void orb::Editor::_actionGetHoldVertex()
{
	m_holdVertex = checkVertexMouseCollision();
	if (m_holdVertex != nullptr && m_mode == ORB_EDITM_NORMAL)
	{	
		m_mode = ORB_EDITM_WALL_DRAG;
	}
	_actionUpdateHoldVertex();
}

void orb::Editor::_actionUpdateHoldVertex()
{
	if (m_holdVertex != nullptr && m_mode == ORB_EDITM_WALL_DRAG)
	{
		double xpos, ypos;
		glfwGetCursorPos(m_window->getWindowPointer(), &xpos, &ypos);
		vec2 transformedPos(roundToHundredths((xpos + offset.x) / m_zoom), roundToHundredths((ypos + offset.y) / m_zoom));

		m_holdVertex->getVertexPointer()->setPosition(transformedPos);
	}
}

void orb::Editor::_actionDisableHoldVertex()
{
	if (m_mode == ORB_EDITM_WALL_DRAG)
	{
		m_mode = ORB_EDITM_NORMAL;
	}
}

void orb::Editor::_actionCompileMap()
{
	logMessage(std::endl << std::endl);
	logMessage("now compiling map");
	double startTime = glfwGetTime();
	m_bsp.CompileMapData(m_sectors);
	double endTime = glfwGetTime();
	logMessage("finished compiling map in " << (endTime - startTime) * 1000 << "ms"<< std::endl);
}
