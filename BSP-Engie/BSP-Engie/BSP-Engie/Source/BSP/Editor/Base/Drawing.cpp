#include "../Editor.h"
#include "../../../Core/ErrorHandle.h"

void orb::Editor::drawGrid()
{

	//set the states
	GLfnc(glLineWidth(1));
	GLfnc(glViewport(0, 0, m_window->getWidth(), m_window->getHeight()));
	GLfnc(glLoadIdentity());
	GLfnc(glOrtho(0, m_window->getWidth(), m_window->getHeight(), 0, 0, 1));
	GLfnc(glMatrixMode(GL_MODELVIEW));


	//horizontal
	for (int y = 0; y < (m_window->getHeight() + offset.y) / m_zoom; y += m_cellSize)
	{

		if (y % m_blockSize == 0)
			glColor3f(m_blockColor.x, m_blockColor.y, m_blockColor.z);
		else
			glColor3f(m_cellColor.x, m_cellColor.y, m_cellColor.z);

		glBegin(GL_LINES);
		glVertex3f(0, transformY(y), 0.0);
		glVertex3f(m_window->getWidth(), transformY(y), 0.0);
		glEnd();
	}
	//vertical
	for (int x = 0; x < (m_window->getWidth() + offset.x) / m_zoom; x += m_cellSize)
	{

		if (x % m_blockSize == 0)
			glColor3f(m_blockColor.x, m_blockColor.y, m_blockColor.z);
		else
			glColor3f(m_cellColor.x, m_cellColor.y, m_cellColor.z);

		glBegin(GL_LINES);
		glVertex3f(transformX(x), 0, 0);
		glVertex3f(transformX(x), m_window->getHeight(), 0);
		glEnd();
	}


	GLfnc(glFlush());
}

void orb::Editor::drawVerticies()
{
	for (int i = 0; i < m_sectors.size(); i++)
	{
		m_sectors[i].draw(m_window->getWidth(), m_window->getHeight(), offset, m_zoom, m_cellSize);
	}

	if(m_mode == ORB_EDITM_LINE_EDIT)
		_drawTemporaryVerticies();
}

void orb::Editor::_drawTemporaryVerticies()
{

	GLfnc(glLineWidth(3));
	GLfnc(glViewport(0, 0, m_window->getWidth(), m_window->getHeight()));
	GLfnc(glLoadIdentity());
	GLfnc(glOrtho(0, m_window->getWidth(), m_window->getHeight(), 0, 0, 1));
	GLfnc(glMatrixMode(GL_MODELVIEW));

	glColor4f(1, 1, 0, 1);
	glBegin(GL_LINE_STRIP);

	for (int i = 0; i < m_temporaryVertices.size(); i++)
	{
		glVertex3f((m_temporaryVertices[i].getVertex().getPosition().x) * m_zoom - offset.x, (m_temporaryVertices[i].getVertex().getPosition().y) * m_zoom - offset.y, 0);

	}

	//draw the to the mouse
	double xpos, ypos;
	glfwGetCursorPos(m_window->getWindowPointer(), &xpos, &ypos);
	glVertex3f(xpos, ypos, 0);

	glEnd();

	//draw the hitbox for the first one
	if (m_temporaryVertices.size() > 0)
	{
		glColor4f(1, 0, 0, 0.5);
		glBegin(GL_QUADS);

		float halfWidth = m_cellSize / 2;

		glVertex3f((m_temporaryVertices[0].getVertex().getPosition().x - halfWidth) * m_zoom - offset.x, (m_temporaryVertices[0].getVertex().getPosition().y - halfWidth) * m_zoom - offset.y, 0);
		glVertex3f((m_temporaryVertices[0].getVertex().getPosition().x - halfWidth) * m_zoom - offset.x, (m_temporaryVertices[0].getVertex().getPosition().y + halfWidth) * m_zoom - offset.y, 0);
		glVertex3f((m_temporaryVertices[0].getVertex().getPosition().x + halfWidth) * m_zoom - offset.x, (m_temporaryVertices[0].getVertex().getPosition().y + halfWidth) * m_zoom - offset.y, 0);
		glVertex3f((m_temporaryVertices[0].getVertex().getPosition().x + halfWidth) * m_zoom - offset.x, (m_temporaryVertices[0].getVertex().getPosition().y - halfWidth) * m_zoom - offset.y, 0);

		glEnd();
	}


	GLfnc(glFlush());
}
