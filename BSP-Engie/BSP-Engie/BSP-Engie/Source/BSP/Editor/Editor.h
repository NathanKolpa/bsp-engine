#pragma once
#include "../../../Include.h"
#include "../../Renderer/Data/Wall3D.h"
#include "../../Core/Types.h"
#include "../Data/SectorArray.h"
#include "../Base/BinarySpacePartition.h"

namespace orb//hier moet je zo'n binary tree kunnen generaten met alle assets erbij
{
	class Editor
	{
	private:
		Window* m_window;

		//transformation
		float m_zoom = 1;
		vec2 offset;

		//grid
		int m_blockSize = 500;
		int m_cellSize = 100;
		vec3 m_cellColor = vec3(0.411, 0.411, 0.411);
		vec3 m_blockColor = vec3(0, 0.109, 0.560);

		//editor states
		int m_mode = ORB_EDITM_NORMAL;
		int m_state = ORB_EDITS_NORMAL;
		int m_view = ORB_EDITV_2D;
		LinkedVertex* m_holdVertex = nullptr;

		//map data
		SectorArray m_sectors;
		std::vector<LinkedVertex> m_temporaryVertices;
		BinarySpacePartition m_bsp;

	public:
		Editor(Window& window);
		~Editor();
	public:
		void _actionPlaceWall();//add a vertex to the temporary verticies
		void _actionToggleLineEditing();//enable line editing
		void _actionAddSector();//add the temporary vertecies to the array
		void _actionGetHoldVertex();//get a hold vertex if it is avalible
		void _actionUpdateHoldVertex();//updates the hold vertex to the mouse
		void _actionDisableHoldVertex();
		void _actionCompileMap();
	private:
		void keyboardInput();
		float transformX(float a);
		float transformY(float a);
		vec2 screenMouse();

		void prossesWallColors();

		void drawGrid();
		void drawVerticies();
		void _drawTemporaryVerticies();
		LinkedVertex* checkVertexMouseCollision();
	public:
		void run();
	};
}

