#include "Editor.h"
#include "../../Core/ErrorHandle.h"
#include "../../Core/Utils/Time.h"


orb::Editor::Editor(Window& window)
{
	m_window = &window;
	logMessage(std::endl << "=====[controls]=====" << std::endl
		<< "N - new sector" << std::endl
		<< "C - compile to bsp" << std::endl
		<< "R - cycle view");


	glfwSetWindowUserPointer(m_window->getWindowPointer(), this);

	auto keyCallBack = [](GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		if (action == GLFW_RELEASE)
		{
			if (key == GLFW_KEY_N)
			{
				static_cast<Editor*>(glfwGetWindowUserPointer(window))->_actionToggleLineEditing();
			}

			if (key == GLFW_KEY_C)
			{
				static_cast<Editor*>(glfwGetWindowUserPointer(window))->_actionCompileMap();
			}
		}
	};

	auto MouseCallBack = [](GLFWwindow* window, int button, int action, int mods)
	{

		if (action == GLFW_PRESS)
		{
			if (button == GLFW_MOUSE_BUTTON_LEFT)
				static_cast<Editor*>(glfwGetWindowUserPointer(window))->_actionGetHoldVertex();
		}

		if (action == GLFW_RELEASE)
		{
			if (button == GLFW_MOUSE_BUTTON_LEFT)
			{
				static_cast<Editor*>(glfwGetWindowUserPointer(window))->_actionPlaceWall();
				static_cast<Editor*>(glfwGetWindowUserPointer(window))->_actionDisableHoldVertex();
			}

			if (button == GLFW_MOUSE_BUTTON_RIGHT)
			{
			}
		}
	};

	glfwSetKeyCallback(m_window->getWindowPointer(), keyCallBack);
	glfwSetMouseButtonCallback(m_window->getWindowPointer(), MouseCallBack);
}


orb::Editor::~Editor()
{
}

float orb::Editor::transformX(float a)
{
	return a * m_zoom - offset.x;
}

float orb::Editor::transformY(float a)
{
	return a * m_zoom - offset.y;
}

vec2 orb::Editor::screenMouse()
{
	double xpos, ypos;
	glfwGetCursorPos(m_window->getWindowPointer(), &xpos, &ypos);
	return vec2(((xpos + offset.x) / m_zoom), ((ypos + offset.y) / m_zoom));
}


void orb::Editor::prossesWallColors()
{
}

orb::LinkedVertex *orb::Editor::checkVertexMouseCollision()
{
	for (int i = 0; i < m_sectors.size(); i++)
	{
		LinkedVertex* current = m_sectors[i].getInterSectingVertex(m_window->getWindowPointer(), m_cellSize, offset, m_zoom);
		if (current != nullptr)
		{
			return current;
		}
	}

	return nullptr;
}

void orb::Editor::run()
{

	m_zoom = 0.25;

	while (m_window->isActive())
	{
		m_window->prepare();
		keyboardInput();
		
		_actionUpdateHoldVertex();

		drawGrid();
		drawVerticies();
		m_bsp.draw(m_window->getWidth(), m_window->getHeight(), offset, m_zoom);
		m_window->update();
	}
}