#pragma once
#include "../Base/BinarySpacePartition.h"
#include "../Data/Section.h"

namespace orb
{
	class BSPCompiler
	{
	public:
		void compile(Sector map);
	private:
		void groupWalls();
		void compileGroup();
	};
}