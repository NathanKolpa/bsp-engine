#pragma once
#include <vector>
#include "../Data/SectorArray.h"
#include <functional>
#include <memory>

namespace orb
{
	struct Wall
	{
		Vertex point1;
		Vertex point2;
		bool isPortal = false;
	};

	struct node
	{
		std::shared_ptr<node> left;
		std::shared_ptr<node> right;
		Wall wall;
		std::vector<Wall> currentlyHoldedWalls;//this must always be 0 when returning (if it is not then it has not been compiled correctly)
	};

	class BinarySpacePartition
	{
	private:
		std::shared_ptr<node> m_topNode;
	public:
		BinarySpacePartition();
		~BinarySpacePartition();
	private:
		void deleteTree();
	public:
		void foreachInTree(std::function<void(std::shared_ptr<node>)>action);
		void draw(int windowWidth, int windowHeight, vec2 offset, float zoom);
		void CompileMapData(SectorArray& data);
	};
}

