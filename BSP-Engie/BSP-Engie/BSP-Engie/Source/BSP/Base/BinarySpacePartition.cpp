#include "BinarySpacePartition.h"
#include"../../Core/ErrorHandle.h"
#include "../../Core/Utils/Utils.h"
#include <algorithm>


orb::BinarySpacePartition::BinarySpacePartition()
{
}


orb::BinarySpacePartition::~BinarySpacePartition()
{
	deleteTree();
}

void visitNode(std::shared_ptr<orb::node> currentNode, std::function<void(std::shared_ptr<orb::node>)>& action)
{//the lambda argument may cause stack memory issius
	if (currentNode->left != nullptr)
	{
		visitNode(currentNode->left, action);
	}

	if (currentNode->right != nullptr)
	{
		visitNode(currentNode->right, action);
	}

	action(currentNode);
}

void orb::BinarySpacePartition::foreachInTree(std::function<void(std::shared_ptr<node>)> action)
{
	if (m_topNode == nullptr)
		return;

	visitNode(m_topNode, action);
}

void orb::BinarySpacePartition::deleteTree()
{
	logMessage("deleting old bsp tree...");

	foreachInTree([&](std::shared_ptr<node> currentNode)
	{
		currentNode.reset();
	});
}



void orb::BinarySpacePartition::draw(int windowWidth, int windowHeight, vec2 offset, float zoom)
{
	if (m_topNode == nullptr)
		return;

	GLfnc(glLineWidth(3));
	GLfnc(glViewport(0, 0, windowWidth, windowHeight));
	GLfnc(glLoadIdentity());
	GLfnc(glOrtho(0, windowWidth, windowHeight, 0, 0, 1));
	GLfnc(glMatrixMode(GL_MODELVIEW));

	std::vector<vec4> colors =
	{
		vec4(1,0,0,1),
		vec4(0.5,0.5,0,1),
		vec4(0,1,0,1),
		vec4(0,0.5,0.5,1),
		vec4(0,0,1,1),
		vec4(0.5, 0, 0.5, 1)
	};

	int colorIndex = 0;

	foreachInTree([&](std::shared_ptr<node> currentNode)
	{
		Wall wall = currentNode->wall;

		glLineWidth(5);
		glColor4f(colors[colorIndex].x, colors[colorIndex].y, colors[colorIndex].z, colors[colorIndex].w);
		glBegin(GL_LINES);

		glVertex3f(wall.point1.getPosition().x * zoom - offset.x, wall.point1.getPosition().y * zoom - offset.y, 0);
		glVertex3f(wall.point2.getPosition().x * zoom - offset.x, wall.point2.getPosition().y * zoom - offset.y, 0);

		glEnd();
		colorIndex++;
		if (colorIndex > colors.size() - 1)
			colorIndex = 0;
	});

	GLfnc(glFlush());
}


std::vector <orb::Wall> getAllWalls(orb::SectorArray& data)
{
	std::vector<orb::Wall> walls;

	//push all the walls in one big array
	for (int j = 0; j < data.size(); j++)
	{
		for (int i = 0; i < data[j].getVerticies().size(); i++)
		{

			if (data[j].getVerticies().size() > 1)
			{
				if (i < data[j].getVerticies().size() - 1)
				{//current and next
					orb::Wall current;
					current.point1 = data[j].getVerticies()[i].getVertex();
					current.point2 = data[j].getVerticies()[i + 1].getVertex();

					walls.push_back(current);
				}

				else if(data[j].getVerticies().size() != 2)
				{//last and first
					orb::Wall current;
					current.point1 = data[j].getVerticies().back().getVertex();
					current.point2 = data[j].getVerticies().front().getVertex();

					walls.push_back(current);
				}
			}
		}
	}

	return walls;
}

void splitWalls(std::vector<orb::Wall>& data, int refrenceIndex, std::vector<orb::Wall>& left, std::vector<orb::Wall>& right)
{
	//            			right
	//    ___    | 1
	//           |
 //---p1=========|==p2------------------------
	//           |
	//	/        | 2
	// /         |			left

	//check if the wall intersect the refrence				(both verticies must be on one side)
	//if not then add the wall to that side
	//else split the wall and add both sides to the corresponding side
	//add the splitter plane as a portal

	right.clear();
	left.clear();

	if (data.size() <= 1)
		return;

	vec2 refrenceP1 = data[refrenceIndex].point1.getPosition();
	vec2 refrenceP2 = data[refrenceIndex].point2.getPosition();

	for (int i = 0; i < data.size(); i++)
	{
		if (i != refrenceIndex)
		{

			int vertex1Side = orb::getSideOfPoint(refrenceP1, refrenceP2, data[i].point1.getPosition());
			int vertex2Side = orb::getSideOfPoint(refrenceP1, refrenceP2, data[i].point2.getPosition());

			if (vertex1Side == ORB_LEFT && vertex2Side == ORB_LEFT)//if both are on the left
			{
				left.push_back(data[i]);
			}

			else if (vertex1Side == ORB_RIGHT && vertex2Side == ORB_RIGHT)//if both are on the right
			{
				right.push_back(data[i]);
			}
			//xor
			else if (!vertex1Side == ORB_MIDDLE != vertex2Side == !ORB_MIDDLE)//if one of the vertex is on the middle(doenst count as intersecting
			{
				if (vertex1Side != ORB_MIDDLE)
				{
					if (vertex1Side == ORB_LEFT)
						left.push_back(data[i]);
					else
						right.push_back(data[i]);
				}
				else
				{
					if (vertex2Side == ORB_LEFT)
						left.push_back(data[i]);
					else
						right.push_back(data[i]);
				}
			}

			else if (vertex1Side == ORB_MIDDLE && vertex2Side == ORB_MIDDLE)//if it is on the line
			{
				left.push_back(data[i]);//left is just picked beacuse random
			}

			else//the line is intersecting and must be split... pathetic
			{
				vec2 intersectingPoint = orb::LineLineIntersect(refrenceP1, refrenceP2, data[i].point1.getPosition(), data[i].point2.getPosition());

				orb::Wall wall1;									//    wall1 | wall2
				wall1.point1 = data[i].point1.getPosition();		//p1========|=======p2
				wall1.point2 = intersectingPoint;

				orb::Wall wall2;
				wall2.point1 = data[i].point2.getPosition();
				wall2.point2 = intersectingPoint;

				if (vertex1Side == ORB_LEFT)
				{
					left.push_back(wall1);
					right.push_back(wall2);
				}
				else
				{
					left.push_back(wall2);
					right.push_back(wall1);
				}

			}
		}
	}
}


int nodeAmount = 0;
void proscessNode(std::shared_ptr<orb::node> current)
{
	if (current->currentlyHoldedWalls.size() > 0)
	{
		current->wall = current->currentlyHoldedWalls[0];
		nodeAmount++;
		if (current->currentlyHoldedWalls.size() > 1)
		{
			std::vector<orb::Wall> right;
			std::vector<orb::Wall> left;
			splitWalls(current->currentlyHoldedWalls, 0, left, right);

			if (right.size() > 0)
			{
				current->right = std::make_shared<orb::node>();
				current->right->currentlyHoldedWalls = right;
				proscessNode(current->right);
			}
			if (left.size() > 0)
			{
				current->left = std::make_shared<orb::node>();
				current->left->currentlyHoldedWalls = left;
				proscessNode(current->left);
			}
		}
	}
}

void orb::BinarySpacePartition::CompileMapData(SectorArray& data)
{
	if (data.size() <= 0 && data[0].getVerticies().size() > 1)
		return;


	deleteTree();
	nodeAmount = 0;

	std::vector<Wall> walls = getAllWalls(data);

	m_topNode = std::make_shared<node>();
	m_topNode->currentlyHoldedWalls = walls;
	m_topNode->wall = walls[0];

	//split de walls
	//de refrence is word ook de node wall
	//stop de left en right in de goede children
	logMessage("now generating walls...");
	proscessNode(m_topNode);								//alleen vertex 1 word past het aan'
	logMessage("tree has now " << nodeAmount << " walls");
}
