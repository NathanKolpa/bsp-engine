#version 410

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_textureCoord;
layout(location = 2) in vec3 in_normal;

uniform mat4 uni_projection;

uniform mat4 uni_translate;
uniform mat4 uni_scale;
uniform mat4 uni_rotationX;
uniform mat4 uni_rotationY;
uniform mat4 uni_rotationZ;

mat4 rotation = uni_rotationX * uni_rotationY * uni_rotationZ;
mat4 transform = uni_projection * uni_translate * uni_scale * rotation;

void main()
{
	gl_Position = transform * vec4(in_position.xyz, 1.0);
}
